<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

$addon = $displayData['addon'];

// Visibility
$custom_class = (isset($addon->settings->hidden_md) && $addon->settings->hidden_md) ? 'jwpf-hidden-md jwpf-hidden-lg ' : '';
$custom_class .= (isset($addon->settings->hidden_sm) && $addon->settings->hidden_sm) ? 'jwpf-hidden-sm ' : '';
$custom_class .= (isset($addon->settings->hidden_xs) && $addon->settings->hidden_xs) ? 'jwpf-hidden-xs ' : '';

// Animation
$addon_attr = '';
if(isset($addon->settings->global_use_animation) && $addon->settings->global_use_animation) {
    if(isset($addon->settings->global_animation) && $addon->settings->global_animation) {
        $custom_class .= ' jwpf-wow ' . $addon->settings->global_animation . ' ';
        if(isset($addon->settings->global_animationduration) && $addon->settings->global_animationduration) {
            $addon_attr .= ' data-jwpf-wow-duration="' . $addon->settings->global_animationduration . 'ms" ';
        }
        if(isset($addon->settings->global_animationdelay) && $addon->settings->global_animationdelay) {
            $addon_attr .= 'data-jwpf-wow-delay="' . $addon->settings->global_animationdelay . 'ms" ';
        }
    }
}

$html = '<div id="jwpf-addon-'. $addon->id .'" class="'. $custom_class .'clearfix" '.  $addon_attr .'>';

if(isset($addon->settings->global_use_overlay) && $addon->settings->global_use_overlay){
    $html .= '<div class="jwpf-addon-overlayer"></div>';
}
echo $html;
